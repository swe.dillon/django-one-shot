from django.shortcuts import redirect, render
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm

# Create your views here.


def todo_list_list(request):
    all_TodoLists = TodoList.objects.all()
    context = {"TodoList": all_TodoLists}
    return render(request, "todos/templatelistview.html", context)


def todo_list_detail(request, id):
    model_instance = TodoList.objects.get(id=id)
    context = {"Tasks": model_instance}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoListForm(instance=model_instance)

    context = {"form": form}

    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("http://localhost:8000/todos/")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItem(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todos/detail.html", id=model_instance.id)

    else:
        form = TodoItem()

    context = {"task": form}

    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    model_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItem(request.POST, instance=model_instance)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoItem(instance=model_instance)

    context = {"form": form}

    return render(request, "todos/edit.html", context)
