from django.contrib import admin
from .models import TodoList, TodoItem

# Register your models here.

admin.site.register(TodoList)
admin.site.register(TodoItem)


class TodoList(admin.ModelAdmin):
    list_display = ["Name", "ID"]


class TodoItem(admin.ModelAdmin):
    list_display = ["Task", "Due Date", "Completed?"]
